import unittest
import asyncio

from unit_tests.test_user_creation import TestUserCreation
from unit_tests.test_login import TestLogin

import no_app


if __name__ == '__main__':
    no_app.startup_event()
    unittest.main()
