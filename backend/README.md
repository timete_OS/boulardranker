FaceBoule Backend
==================



## How to use it

You will need python 3.10 (maybe 3.8 works, but it's not sure). Simply run those commands to install depedencies and run the API + Database.

```sh
pip install -r requirements.txt
python no_main.py
```

On the `.env` file, you will find the database configuration, along with others parameters. If you wan't to be able to send confirmation email, you will have to create a `.keys` file with your mail password.

To contact the API, most of GET requests or very simple. You can essentialy extract all information you want without any tokens or complications.

```sh
curl -X 'GET' 'https://faceboule/games'
```

However, if you desire to change or create things, more verbose requests are necessary.

```sh
curl -X PUT https://faceboule/game -H  'token: 1234' \
-H "Content-Type: application/json"
-d '{
  "winner": {
    "pseudo": "string",
    "elo": 1,
    "id": "8aaff38b-4336-49dd-aa98-08c82d285c52",
    "first_name": "string",
    "last_name": "string",
    "member_since": "2022-10-14"
  },
  "looser": {
    "pseudo": "string",
    "elo": 1,
    "id": "8aaff38b-4336-49dd-aa98-08c82d285c52",
    "first_name": "string",
    "last_name": "string",
    "member_since": "2022-10-14"
  },
  "winner_validation": false,
  "looser_validation": false,
  "is_valid": false,
  "comment": "string",
  "date": "2022-10-14T19:45:06.690Z",
  "fanny": false,
  "contre": false
}'
```

But honestly you should just go to [FaceBoule/docs](https://faceboule/docs).

## General use of the API

```mermaid
graph LR;
    User1_log_in-- send a token ---> Instanciate_Game;
    Instanciate_Game --> AskForConfirmation --> ConfirmGame;

    User2_log_in-- send a token ---> ConfirmGame-->UpdateELO;
    UpdateELO-- save the game --->id1[(Database)];
```




## General Class organization

Here a rather complex (lol) class diagram


## List of important endpoints and why you do not want to mess with them

There are two PUT endpoint

* **/user** : in order to create a new user
* **/email_confirmation** : where to put the confirmation code
*  **/game** : in order to create one game

Importants GET endpoints are **/login**, **/games** and **/users**, unsurprisingly.

Those endpoints aren't very likely to move, as they are essential to the app.


Generally speaking, you should find all information you need on generated [documentation](https://faceboule/docs). If the server is down, just start your own.

# Stuff to do

### FUCKING LOGS !!!

I should have thought of this

### MORE TESTS

- contract testing
- creation of game
- creation of user
- user modification
- game confirmation

### emails

- differents errors must be catch, and transmitted to the front end
- ideally, a dedicated adress would be better...

### users

- being able to set more than the pseudo would be great


## Précision

Idéalement, les mots de passe devraient être stocké dans une table à part. On accèderai au mdp en hachant l'identifiant avec un salt. Ainsi, si la base fuite, non seulement les mdp sont hachés, mais en plus impossible de faire le lien entre les identifiants et les mdp. Donc même si des gens utilisent des mdp très faibles qui se font bruteforce en 2s, ils seront plus ou moins sauvés.


je sais très bien que je devrais passer par des bibliothèques dédiées, voir un service externe, pour gérer l'authentification. Cependant, l'idée c'est aussi de me former un peu en mettant les mains dans le cambouis histoire de voir à quoi ça ressemble.