import hashlib
from datetime import datetime, date
from uuid import uuid4

from sqlalchemy import and_, or_, desc
from sqlalchemy.sql import case

import schemas
from game import Game
from user import User, Token


class ORM:

    def __init__(self) -> None:
        pass

    def get_by_id(self,  session, ObjectType, id: str) -> User:
        return session.query(ObjectType).filter(ObjectType.id == id).first()

    def get_all(self, ObjectType, session, skip: int = 0, limit: int = 100, order = None):
        if order is not None:
            return session.query(ObjectType)\
                .order_by(desc(order))\
                    .offset(skip)\
                        .limit(limit).all()
        return session.query(ObjectType)\
                .offset(skip)\
                    .limit(limit).all()

    #  sqlalchemy.orm.Query.exists() here may be useful ? nope
    def get_with(self, session, ObjectType, limit: int = 100, **user_attribute):
        return session.query(ObjectType).filter(
            and_(
                getattr(ObjectType, key) == item for key, item in user_attribute.items()
            )
        ).limit(limit).all()

    """ def get_with_or(self, session, ObjectType, limit: int = 100, **user_attribute):
        return session.query(ObjectType).filter(
        or_(
            getattr(ObjectType, key) == item for key, item in user_attribute.items()
            )
        ).limit(limit).all() """

    def get_user(self, session,  user_id: str) -> User:
        return session.query(User).filter(User.id == user_id).first()

    def get_games_from_user(self, session, user_id: str):  # -> list[Game]:
        return session.query(Game).filter(
            (Game.looser_id == user_id) | (Game.winner_id == user_id)
        ).all()

    def create_user(self, session, user_creation: schemas.UserCreate):

        user = User(email=user_creation.email,
                    first_name=user_creation.first_name,
                    last_name=user_creation.last_name,
                    member_since=date.today(),
                    elo=1000)

        user.id = str(uuid4())
        attributes = vars(user)
        pswd = user_creation.pswd.encode()
        attributes.pop('_sa_instance_state')
        attributes["hashed_password"] = hashlib.sha256(pswd).hexdigest()
        print(attributes)
        session.session_user = User(**attributes)
        session.add(session.session_user)
        session.commit()
        session.refresh(session.session_user)
        return session.session_user

    def create_token(self, session, token: Token):
        session.add(token)
        session.commit()
        session.refresh(token)
        return token

    def update_user(self, session, user: User, **kwargs):
        session.query(User).\
            filter(User.id == user.id).\
            update(kwargs)
        session.commit()
        return user

    # def update_users(self, session, users):
    #     attributes = [User.elo,
    #                     User.email,
    #                     User.pseudo,
    #                     User.first_name,
    #                     User.last_name
    #                 ]
        
    #     updates = {attribute : case(values_dict, value=User.id) for attribute in attributes}
    #     session.query(User) \
    #         .filter(User.id.in_(user_mapper.keys())) \
    #         .update(updates)
        

    def create_game(self, session, game: schemas.Game):
        attributes = vars(game)
        attributes["id"] = str(uuid4())

        # can't find any other way to pass from schemas.Game to Game
        # but it's better, because it ensure that related players are
        # indeed in the database, without any conflicts
        attributes["winner"] = session.query(User).filter(
            User.id == game.winner.id).first()
        attributes["looser"] = session.query(User).filter(
            User.id == game.looser.id).first()

        session.session_game = Game(**attributes)
        session.add(session.session_game)
        session.commit()
        print(" ça sert à quoi même cette ligne en dessous ? ")
        session.refresh(session.session_game)
        return session.session_game

    def unconfirmed_games(self, session, user_id):
        return session.query(Game).filter(
            or_(
                and_(Game.winner_id == user_id,
                     Game.winner_validation == False),
                and_(Game.looser_id == user_id,
                     Game.looser_validation == False)
            )
        )

    def update_game(self, session, game_id: str):
        pass

    # only called at API startup, in order to do some testing later on

    def populate(self, session):
        timo = User(**{'id': 1,
                       'first_name': "Timo",
                       'last_name': "Bidouille",
                       'email': "timo.bidouille@eleve.ensai.fr",
                       'hashed_password': hashlib.sha256("canard".encode()).hexdigest(),
                       'elo': 1000,
                       'member_since': date(2021, 9, 1),
                        })

        splinter = User(**{'id': 2,
                           'first_name': "Splinter",
                           'last_name': "Elrato",
                           'email': "splinter.elrato@eleve.ensai.fr",
                           'hashed_password': hashlib.sha256("je rage".encode()).hexdigest(),
                           'member_since': date(2021, 10, 12),
                           'elo': 1000})

        cecile = User(**{'id': 3,
                         'first_name': "Cecile",
                         'last_name': "Boubou",
                         'email': "cecile.boubou@eleve.ensai.fr",
                         'hashed_password': hashlib.sha256("tiph_le_sang".encode()).hexdigest(),
                         'member_since': date(2022, 3, 21),
                         'elo': 1200})
        session.expire_on_commit = False

        for user in [timo, splinter, cecile]:
            session.merge(user)
        session.commit()

        print("ELO WORLD", timo.elo, cecile.elo, splinter.elo)

        partie0 = Game(id='première game',
                       winner=timo,
                       looser=splinter,
                       date=datetime.fromisoformat("2021-05-15T15:54:48"),
                       is_valid=True,
                       winner_validation=True,
                       looser_validation=True)

        partie1 = Game(id='deuxieme game',
                       winner=cecile,
                       looser=splinter,
                       date=datetime.fromisoformat("2021-06-15T15:54:48"),
                       looser_validation=True)

        partie2 = Game(id='troisieme game',
                       winner=cecile,
                       looser=timo,
                       date=datetime.fromisoformat("2021-06-15T16:00"),
                       looser_validation=True)

        partie3 = Game(id='quatrieme game',
                       winner=timo,
                       looser=cecile,
                       date=datetime.fromisoformat("2021-07-15T16:00"),
                       is_valid=True,
                       fanny=True,
                       looser_validation=True,
                       winner_validation=True,
                       comment="Quelle garce cette fille")
        # don't know why I have to set this as False

        for game in (partie0, partie1, partie2, partie3):
            session.merge(game)
        session.commit()

        partie0.update_all_elo(session)


        token = Token(value="coin coin", owner_id=1)
        session.merge(token)
        session.commit()
