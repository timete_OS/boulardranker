fastapi==0.88.0
fastapi_utils==0.2.1
pydantic==1.10.2
python-dotenv==0.21.0
SQLAlchemy==1.4.36
uvicorn==0.17.6
