import React from 'react';


const globalState = {
    // api_url: "http://127.0.0.1:8000",
    api_url : "https://front.faceboule.fr", //or "http://127.0.0.1:8000", if using localhost
  };


const globalStateContext = React.createContext(globalState);

export default globalStateContext