import React, { useState, useEffect } from 'react';
import { getDevice }  from 'framework7/lite-bundle';
import { Storage } from '@capacitor/storage';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter
} from 'framework7-react';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import store from '../js/store';

const MyApp = () => {

  // Login screen demo data
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const device = getDevice();
  // Framework7 Parameters
  const f7params = {
    name: 'FaceBoule', // App name
      theme: 'auto', // Automatic theme detection


      id: 'io.framework7.myapp', // App bundle ID
      // App store
      store: store,
      // App routes
      routes: routes,

      // Input settings
      input: {
        scrollIntoViewOnFocus: device.capacitor,
        scrollIntoViewCentered: device.capacitor,
      },
      // Capacitor Statusbar settings
      statusbar: {
        iosOverlaysWebView: true,
        androidOverlaysWebView: false,
      },
  };
  const alertLoginData = () => {
    f7.dialog.alert('Username: ' + username + '<br>Password: ' + password, () => {
      f7.loginScreen.close();
    });
  }
  f7ready(() => {

    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });

  /*if (checkName) {

  console.log("AUX ORIGINES, VOILA LA VALEUR DE TOKEN : ", token, (token !== Promise.resolve(null)));
  const b = token.then(token === null)
  if (!b) {
    console.log("b est false, token est : ", token)
    return (
      <App { ...f7params } >
            // home
          <View url="/home/" />
          </App>)} */
    
    
    return(
    <App { ...f7params } >
      {/* login */}
      <View id="tab-1" tab url='/home/' />
    </App>)
}
export default MyApp;
