import React, { useState } from 'react';
import {
    f7,
    Page,
    LoginScreenTitle,
    List,
    ListInput,
    ListButton,
    BlockFooter,
    BlockHeader,
    Link    
  } from 'framework7-react';

import { Storage } from '@capacitor/storage';

import globalStateContext from '../js/global_state';



let login_info = {
    email : '',
    pswd : ''
  }


async function connection(login_info, api_url)
{
    const params = new URLSearchParams(login_info).toString()
    console.log("ici les paramètres : ", params)
    console.log("url de l'api : ", api_url)
    const rawResponse = await fetch(api_url + "/login?" + params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        });
        const content = await rawResponse.json();
        
        console.log(content);
        return content
}



  
function LoginPage({ f7router }) {
    // ici on veut rafraichir le contexte
    const {api_url} = React.useContext(globalStateContext);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const signIn = async() => {
        login_info["pswd"] = password
        login_info["email"] = username
        const prout = await connection(login_info, api_url);

        console.log("hourra", prout)

        if (prout.access_token != null)
        {
            console.log("youpi")
            await Storage.set({
                key: 'access_token',
                value: prout.access_token,
            });
            await Storage.set({
                key: 'id',
                value: prout.id,
            });
            f7router.navigate('/home/');
        }
        else
        {         
            f7.dialog.alert(`La combinaison identifiants / mot de passe n'est pas valide`, () => {
            f7router.back(); 
            });  
        }
        // f7.dialog.alert(`Username: ${username}<br>Password: ${password}`, () => {
        // f7router.back();
        // });
    };

    //console.log(token)

    return (
        <Page noToolbar noNavbar noSwipeback loginScreen>
    <LoginScreenTitle>FaceBoule</LoginScreenTitle>

    <BlockHeader>La plateforme de référence pour maintenir un classement des joueurs et joueuses de billard de l'ENSAI</BlockHeader>

    <List form>
        <ListInput
        label="Username"
        type="text"
        placeholder="Votre courriel"
        value={username}
        onInput={(e) => {
            setUsername(e.target.value);
        }}
        />
        <ListInput
        label="Password"
        type="password"
        placeholder="Mot de passe"
        value={password}
        onInput={(e) => {
            setPassword(e.target.value);
        }}
        />
    </List>
    <List>
        <ListButton onClick={signIn}>Connexion</ListButton>
        <ListButton href='/create_account/'>Créer un compte</ListButton>

        <BlockFooter>
        Vous devez posséder une adresse ensai pour vous inscrire
        <br />
        (Pas du tout)
        </BlockFooter>
        <BlockFooter>
    <div>
        Vous pouvez contribuer au developpement sur <a class="link external"  target="_blank" href="https://gitlab.com/timete_OS/FaceBoule">GitLab</a>
     </div>
        </BlockFooter>
    </List>
        </Page>
    );
};


export default LoginPage;
