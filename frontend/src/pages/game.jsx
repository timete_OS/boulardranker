import React from 'react';
import {
    Page,
    Navbar,
    List,
    ListInput,
    ListItem,
    BlockTitle,
    Block,
    Card,
    CardFooter,
  } from 'framework7-react';


function Game(game) {

    console.log("game 1 : ", game)
    const D = Math.abs(game.winner_elo - game.looser_elo)
    const pD = 1/(1 + 10**(-D/300))
    const K = 3
    const texte = "par fanny" && game.fany

    const gdate = new Date(game.date);
    console.log(gdate)
    console.log(typeof(gdate))

    return (
        <Page name="game">
        <Navbar title="Page très moche à mieux faire" backLink="Back"></Navbar>
        <Block>
        <h2>Le {gdate.toLocaleString()}: </h2>        
        <font size="+2">Gagnant {texte} : </font>
        <Card>
        {game.winner.first_name} {game.winner.last_name}
        {(game.winner.pseudo != null) &&
        <h2>
        aka {game.winner.pseudo}
        </h2>
        }
        </Card>
        <CardFooter>Variation d'ELO : {game.winner_elo} + {Math.round(K*(1 - pD))}</CardFooter>
        </Block>

        <Block>
        <font size="+2">Perdant</font>
        <Card>
        {game.looser.first_name} {game.looser.last_name}
        {(game.looser.pseudo != null) &&
        <h2>
        aka {game.looser.pseudo}
        </h2>
        }
        </Card>
        <CardFooter>Variation d'ELO : {game.looser_elo} - {Math.round(K*pD)}</CardFooter>
        </Block>
        {game.comment != null &&
        <h1 color='gray'>"{game.comment}"</h1>
        }
    </Page>

    );
}


export default Game;