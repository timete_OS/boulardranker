import React from 'react';
import {
  f7,
  Page,
  Navbar,
  ListInput,
  List,
  NavTitleLarge,
  Button,
  Block
} from 'framework7-react';


import globalStateContext from '../js/global_state';

const account_info = {
  first_name : '',
  last_name: '',
  pswd : '',
  email: ''
}



async function connection(account_info, api_url)
{
    const rawResponse = await fetch(api_url + "/user", {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify( account_info )
        });
        const content = await rawResponse.json();
    
        console.log(content);
}



function CreateAccountPage( {f7router})
{
    const {api_url} = React.useContext(globalStateContext);


    function go_to_with_params()
    {        
        console.log("youpi")
    
        f7router.navigate('/validation_code/', {
            props: {
              mail: account_info.email,
            },
            transition: 'f7-parallax'
          })
    }

    return (
    <Page name="create_account">

        <Navbar large sliding={false} backLink="Back">
            <NavTitleLarge>Rejoindre FaceBoule</NavTitleLarge>
        </Navbar>


        <List inlineLabels noHairlinesMd>
            <ListInput
                label="Email"
                type="email"
                placeholder="prénom.nom@eleve.ensai.fr"
                clearButton
                onChange={e=> account_info["email"] = e.target.value}
                required validate>
            </ListInput>

            <ListInput
                label="Prénom"
                type="text"
                placeholder="michel"
                clearButton
                onChange={e=> account_info["first_name"] = e.target.value}
                >
            </ListInput>

            <ListInput
                label="Nom"
                type="text"
                placeholder="forever"
                clearButton
                onChange={e=> account_info["last_name"] = e.target.value}
                >
            </ListInput>


            <ListInput
                label="Mot de passe"
                type="password"
                placeholder="42QBjT@Iv2i^6, par exemple"
                secureTextEntry={true}
                clearButton
                onChange={e=> account_info["pswd"] = e.target.value}
                >
            </ListInput>
        </List>

        <Block strong>
            <Button outline onClick={
                () => {
                    connection(account_info, api_url)
                    go_to_with_params(f7router)
                    // return <ValidationCodePage email = {account_info.email}/>
                }
                }>créer un compte</Button>
        </Block>
    </Page>
    );
}


export default CreateAccountPage;
