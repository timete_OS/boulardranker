    import React from 'react';
    import {
    Page,
    Toolbar,
    Link,
    Tab,
    Tabs,
    Block,
    List,
    Card,
    CardContent,
    CardFooter,
    ListItem,
    Navbar,
    NavTitle,
    NavTitleLarge
    } from 'framework7-react';

    import globalStateContext from '../js/global_state';

    import { Storage } from '@capacitor/storage';


    function LeaderboardPage ()  {

    const {api_url} = React.useContext(globalStateContext);

    //le paramètre de useState c'est la valeur par défaut des arguments


    const [users, setData] = React.useState([]);
    const [id, setId] = React.useState('');
    const [rivals, setRivals] = React.useState([]);

    React.useEffect(() => {   
        async function getEverything() {
            // You can await here
            const storage = await Storage.get({key : 'id'})
            const id = storage.value
            console.log("l'id", id)
            setId(id)

            const users = await fetch(api_url + "/users")
                .then(res => res.json())

            setData(users)

            fetch(api_url + '/' + id +  "/duels")
            .then(rivals => rivals.json()
            .then(json => setRivals(json)))

            console.log(rivals)

            }

        
        getEverything();
    }, []);    


    return (

    <Page name="Classement">
    <Navbar large sliding={false}>
        <NavTitle sliding>Classements</NavTitle>
        <NavTitleLarge>Classements</NavTitleLarge>
    </Navbar>

    <Toolbar tabbar bottom>
        <Link tabLink="#home" href="/home/">Home</Link>
        <Link tabLink="#leaderboard"  tabLinkActive>Classement</Link>
        <Link tabLink="#profile" href="/profile/">Profil</Link>
    </Toolbar>


        <Tabs animated>
        <Tab id="leaderboard" className="page-content">

            <Card title="TOP 8 ELO ENSAI">
                <CardContent padding={false}>
                    <List medial-list>
                    {users.sort(function (a, b) {
                            return b.elo - a.elo;})
                            .slice(0, 8)
                            .map(element => <ListItem title= {element.first_name} footer={element.last_name} after = {element.elo}/>)}
                    </List>
                </CardContent>
                <Link  href = "/completeRanking/" routeProps={{'user_list' : users}}>
                <CardFooter>
                    {/* <a href = "/completeRanking/" routeProps = {{'user_list': users}}><span>Voir le classement intégral</span></a> */}
                    <span>Voir le classement intégral</span>
                </CardFooter>
                </Link>
            </Card>

            <Card title="Mes Duels">
                <CardContent padding={false}>
                    <List medial-list>
                    {rivals.sort(function (a, b) {
                            return b.total_games - a.total_games;})
                            .slice(0, 8)
                            .map(element => <ListItem title= {element.first_name} footer={element.last_name} after = {Math.round(100*element.victory/element.total_games) + " % de victoire"}/>)}
                    </List>
                </CardContent>
                <Link  href = "/completeRivals/" routeProps={{'rival_list' : rivals}}>
                <CardFooter>
                    {/* <a href = "/completeRanking/" routeProps = {{'user_list': users}}><span>Voir le classement intégral</span></a> */}
                    <span>Voir l'ensemble des rivaux</span>
                </CardFooter>
                </Link>
            </Card>

        </Tab>
        </Tabs>
        </Page>
        );
        }

    export default LeaderboardPage;