import React from 'react';
import {
  Page,
  Navbar,
  List,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Button,
} from 'framework7-react';

import globalStateContext from '../js/global_state';
import GameItem from './game-item';

function AllGamesPage() {

    const {api_url} = React.useContext(globalStateContext);

    const [data, setData] = React.useState([]); //le paramètre de useState c'est la valeur par défaut des arguments
    React.useEffect(() => {fetch(api_url + '/games')
    .then(data => data.json()
    .then(json => setData(json)))},
    []);    

    return (

        <Page name="all_games">
            <Navbar title="Toutes les parties" backLink="Back"></Navbar>

            <List medial-list>
                    {data.
                    sort(function (a, b) {return new Date(b.date) - new Date(a.date);}).
                    map(element => GameItem(element))}    
            </List>
        </Page>

);
}

export default AllGamesPage;

